package com.supakorn.photoapp.module

import com.supakorn.photoapp.main.FullScreenFragment
import com.supakorn.photoapp.main.MainFragment
import org.koin.dsl.module

val fragmentModule = module {
    factory { MainFragment() }
    factory { FullScreenFragment() }
}