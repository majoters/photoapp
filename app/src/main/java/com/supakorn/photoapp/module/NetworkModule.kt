package com.supakorn.photoapp.module

import com.supakorn.photoapp.networking.ImageApi
import com.supakorn.photoapp.networking.ResponseHandler
import com.supakorn.photoapp.networking.okHttp
import com.supakorn.photoapp.networking.retrofit
import org.koin.dsl.module
import retrofit2.Retrofit

const val BASE_URL = "https://jsonplaceholder.typicode.com/"

val networkModule = module {
    single {
        okHttp()
    }
    single {
        retrofit(BASE_URL)
    }
    single {
        get<Retrofit>().create(ImageApi::class.java)
    }
    factory { ResponseHandler() }
}