package com.supakorn.photoapp.module

import com.supakorn.photoapp.repository.ImageRepository
import com.supakorn.photoapp.usecase.GetImageUseCase
import org.koin.dsl.module

val imageModule = module {
    factory { GetImageUseCase(get()) }
    factory { ImageRepository(get(), get()) }
}