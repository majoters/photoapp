package com.supakorn.photoapp.usecase

import com.supakorn.photoapp.model.Image
import com.supakorn.photoapp.networking.Resource
import com.supakorn.photoapp.repository.ImageRepository
import kotlinx.coroutines.flow.Flow

class GetImageUseCase(
    private val imageRepository: ImageRepository
) {
    fun execute(): Flow<Resource<List<Image>>> {
        return imageRepository.getImage()
    }
}