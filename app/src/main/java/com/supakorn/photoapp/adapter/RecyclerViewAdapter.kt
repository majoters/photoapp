package com.supakorn.photoapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.supakorn.photoapp.R
import com.supakorn.photoapp.model.Image
import kotlinx.android.synthetic.main.card_layout.view.*


class RecyclerViewAdapter :
    RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {
    var itemClickListener: ((item: Image, position: Int) -> Unit)? = null
    private var imageDataArrayList = mutableListOf<Image>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        // Inflate Layout
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.card_layout, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        // Set the data to textview and imageview.
        val recyclerData: Image = imageDataArrayList[position]
        holder.bind(recyclerData) {
            itemClickListener?.invoke(imageDataArrayList[position], position)
        }
    }

    override fun getItemCount(): Int {
        // this method returns the size of recyclerview
        return imageDataArrayList.size
    }

    fun setImageList(imageList: List<Image>) {
        imageDataArrayList.clear()
        imageDataArrayList.addAll(imageList)
    }

    // View Holder Class to handle Recycler View.
    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            item: Image,
            itemClickListener: (() -> Unit)?
        ) {
            itemView.item_title.text = item.title
            val picasso = Picasso.Builder(itemView.context)
                .listener { _, _, e -> e.printStackTrace() }
                .build()
            picasso.load(item.thumbnailUrl).placeholder(R.drawable.placeholder_thumb).into(itemView.item_image)
            itemView.setOnClickListener {
                itemClickListener?.invoke()
            }
        }
    }

}