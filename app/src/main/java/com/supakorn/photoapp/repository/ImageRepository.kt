package com.supakorn.photoapp.repository

import com.supakorn.photoapp.model.Image
import com.supakorn.photoapp.networking.Resource
import com.supakorn.photoapp.networking.ResponseHandler
import com.supakorn.photoapp.networking.ImageApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

open class ImageRepository(
    private val imageApi: ImageApi,
    private val responseHandler: ResponseHandler
) {

    fun getImage(): Flow<Resource<List<Image>>> {
        return flow {
            val response = imageApi.getImage()
            emit(responseHandler.handleSuccess(response))
        }.catch { error ->
            emit(responseHandler.handleException(error))
        }
    }
}