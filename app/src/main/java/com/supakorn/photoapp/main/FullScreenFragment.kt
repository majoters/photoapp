package com.supakorn.photoapp.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.supakorn.photoapp.R
import com.supakorn.photoapp.databinding.FullScreenFragmentBinding
import com.supakorn.photoapp.model.Image

class FullScreenFragment : Fragment() {

    private lateinit var binding: FullScreenFragmentBinding
    private lateinit var image: Image

    companion object {
        fun newInstance(item: Image): FullScreenFragment {
            return FullScreenFragment().apply {
                image = item
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FullScreenFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initialLayout()
    }

    private fun initialLayout() {
        val picasso = Picasso.Builder(context)
            .listener { _, _, e -> e.printStackTrace() }
            .build()
        picasso.load(image.url).fit().placeholder(R.drawable.placeholder_full).into(binding.fullScreenIv)
    }

}