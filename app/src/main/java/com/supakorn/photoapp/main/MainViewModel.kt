package com.supakorn.photoapp.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.supakorn.photoapp.model.Image
import com.supakorn.photoapp.networking.Resource
import com.supakorn.photoapp.usecase.GetImageUseCase
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class MainViewModel(
    private val getImageUseCase: GetImageUseCase
) : ViewModel() {
    fun onGetImage(): LiveData<Resource<List<Image>>> = image
    fun onErrorImage(): LiveData<String> = error
    fun onShowLoading(): LiveData<Unit> = loading
    fun onHideLoading(): LiveData<Unit> = loaded

    var image = MutableLiveData<Resource<List<Image>>>()
    var error = MutableLiveData<String>()
    var loading = MutableLiveData<Unit>()
    var loaded = MutableLiveData<Unit>()

    fun getImage() {
        viewModelScope.launch {
            getImageUseCase.execute()
                .onStart { loading.value = Unit }
                .catch { exception ->
                    loaded.value = Unit
                    error.value = exception.message
                }
                .collect {
                    loaded.value = Unit
                    image.value = it
                }
        }
    }
}