package com.supakorn.photoapp.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.supakorn.photoapp.R
import com.supakorn.photoapp.adapter.RecyclerViewAdapter
import com.supakorn.photoapp.databinding.MainFragmentBinding
import com.supakorn.photoapp.networking.Status
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModel()
    private lateinit var binding: MainFragmentBinding
    private val imageAdapter by lazy {
        RecyclerViewAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initialLayout()
        initialViewModel()
        viewModel.getImage()
    }

    private fun initialLayout() {
        val layoutManager = GridLayoutManager(context, 2)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = imageAdapter
        imageAdapter.itemClickListener = { item, _ ->
            val fragment = FullScreenFragment.newInstance(item)
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.containerView, fragment, "secondFragment")
                ?.commit();
        }
    }

    private fun initialViewModel() {
        viewModel.onGetImage().observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { imageList -> imageAdapter.setImageList(imageList) }
                }
                Status.ERROR -> it.message?.let { message -> showError(message) }
                Status.LOADING -> showLoading()
            }
        })
        viewModel.onShowLoading().observe(viewLifecycleOwner, {
            binding.textStatus.visibility = View.VISIBLE
            binding.recyclerView.visibility = View.GONE
        })
        viewModel.onHideLoading().observe(viewLifecycleOwner, {
            binding.textStatus.visibility = View.GONE
            binding.recyclerView.visibility = View.VISIBLE
        })
        viewModel.onErrorImage().observe(viewLifecycleOwner, {
            binding.textStatus.visibility = View.VISIBLE
            binding.recyclerView.visibility = View.GONE
            binding.textStatus.text = it
        })
    }

    @SuppressLint("SetTextI18n")
    private fun showLoading() {
        binding.textStatus.text = "Loading..."
    }

    @SuppressLint("SetTextI18n")
    private fun showError(message: String) {
        binding.textStatus.visibility = View.VISIBLE
        binding.recyclerView.visibility = View.GONE
        binding.textStatus.text = "Error: $message"
    }
}