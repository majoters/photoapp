package com.supakorn.photoapp.networking

import androidx.annotation.WorkerThread
import com.supakorn.photoapp.model.Image
import retrofit2.http.GET

interface ImageApi {

    @WorkerThread
    @GET("albums/1/photos")
    suspend fun getImage(): List<Image>
}