package com.supakorn.photoapp.networking

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}