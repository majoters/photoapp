package com.supakorn.photoapp.usecase

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.supakorn.photoapp.model.Image
import com.supakorn.photoapp.networking.Resource
import com.supakorn.photoapp.repository.ImageRepository
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.runBlockingTest

@RunWith(JUnit4::class)
class GetImageUseCaseTest {
    private lateinit var getImageUseCase: GetImageUseCase
    private lateinit var imageRepository: ImageRepository

    @Before
    fun setUp() {
        imageRepository = mock()
        getImageUseCase = GetImageUseCase(imageRepository)
    }

    @Test
    fun `test getImage when usecase is executed, then response is returned`() = runBlockingTest {
        val imageList = mutableListOf<Image>()
        val image1 = Image().apply {
            albumId = 1
            id = 1
            title = "accusamus beatae ad facilis cum similique qui sunt"
            url = "https://via.placeholder.com/600/92c952"
            thumbnailUrl = "https://via.placeholder.com/150/92c952"
        }
        val image2 = Image().apply {
            albumId = 1
            id = 2
            title = "accusamus beatae ad facilis cum similique qui sunt"
            url = "https://via.placeholder.com/600/92c952"
            thumbnailUrl = "https://via.placeholder.com/150/92c952"
        }
        imageList.add(image1)
        imageList.add(image2)
        whenever(imageRepository.getImage()).thenReturn(flow { emit(Resource.success(imageList.toList())) })
        getImageUseCase.execute().onEach {
            assertEquals(2, it.data?.size)
        }.launchIn(this)
    }

    @Test
    fun `test getImage when usecase is executed, then null is returned`() = runBlockingTest {
        val imageList = mutableListOf<Image>()
        whenever(imageRepository.getImage()).thenReturn(flow { emit(Resource.success(imageList.toList())) })
        getImageUseCase.execute().onEach {
            assertEquals(0, it.data?.size)
        }.launchIn(this)
    }

    @Test
    fun `test getImage when usecase is executed, then error is returned`() = runBlockingTest {
        val imageList = mutableListOf<Image>()
        whenever(imageRepository.getImage()).thenReturn(flow { emit(Resource.error("Server down.",imageList.toList())) })
        getImageUseCase.execute().onEach {
            assertEquals(0, it.data?.size)
        }.launchIn(this)
    }
}